public with sharing class WireTask1Handler {
    @AuraEnabled
    public static List<Account> getAllAccounts(String searchKey){
        try {
            return [SELECT Id, Name FROM Account WHERE Name = :searchKey];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<Account> getAccounts(String searchKey){
        try {
            return [SELECT Id, Name FROM Account WHERE Name = :searchKey];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String createAccount(Account objAccount){
        try {
            if(String.isNotBlank(objAccount.Name) )
            {
                Insert objAccount;
                return 'Account Added Successfully';
            }
            else 
            {
                return 'No Accounts Added';
            }
        
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}