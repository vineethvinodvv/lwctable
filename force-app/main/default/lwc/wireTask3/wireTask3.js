import { LightningElement, track } from 'lwc';
import createAccount from '@salesforce/apex/WireTask1Handler.createAccount';

export default class WireTask3 extends LightningElement {

    value = '';

    @track objAccount = {};
    @track accountMsg;
    @track disabledHandler = false;
    @track showMessage = false;

    get typeOptions() {
        return [
            { label: 'Prospect', value: 'Prospect' },
            { label: 'Customer - Direct', value: 'Customer - Direct' },
            { label: 'Customer - Channel', value: 'Customer - Channel' },
            { label: 'Channel Partner / Reseller', value: 'Channel Partner / Reseller' },
            { label: 'Installation Partner', value: 'Installation Partner' },
            { label: 'Technology Partner', value: 'Technology Partner' },
            { label: 'Other', value: 'Other' },
        ];
    }

    value = '';
    get ownershipOption() {
        return [
            { label: 'Public', Value: 'Public' },
            { label: 'Private', Value: 'Private' },
            { label: 'Subsidiary', Value: 'Subsidiary' },
            { label: 'Other', Value: 'Other' },
        ];
    }

    value = '';
    get industryOption() {
        return [
            { label: 'New', value: 'new' },
            { label: 'In Progress', value: 'inProgress' },
            { label: 'Finished', value: 'finished' },
        ];
    }

    inputHandler(event)
    {
        let fieldName = event.target.name;
        let fieldValue = event.target.value;
        this.objAccount[fieldName] = fieldValue;
    }

    submitHandler(event)
    {
        this.createAccountHandler();
        this.disabledHandler = true;
        this.showMessage = true;

    }

    resetHandler(event)
    {
        this.template.querySelectorAll('lightning-input').forEach(element => 
            {
                if(element.type === 'checkbox' || element.type === 'checkbox-button')
                {
                    element.checked = false;
                }
                else
                {
                    element.value = null;
                }      
          });

          this.disabledHandler  = false;
          this.showMessage = false;
          this.objAccount = {};
    }

    createAccountHandler()
    {
        createAccount({objAccount : this.objAccount})
        .then(result =>{
            this.accountMsg = result;
        }).catch(error =>{
            console.error(error);
        });
    }

}