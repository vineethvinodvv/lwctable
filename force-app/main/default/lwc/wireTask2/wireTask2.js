import { LightningElement, wire, track } from 'lwc';
import getAllAccounts from '@salesforce/apex/WireTask1Handler.getAllAccounts';

export default class WireTask2 extends LightningElement {

    @track searchKey;
    @track accountList;

    getAccountHandler()
    {
        console.log("searchKey "+this.searchKey);
        getAllAccounts({searchKey: this.searchKey}).then(data =>{
            this.accountList = data;
        }).catch(error =>{
            console.log(error);
        })
    }

    searchKeyHandler(event){
        this.searchKey = event.target.value;
    }

    searchHandler(event)
    {
        this.getAccountHandler();
    }

}