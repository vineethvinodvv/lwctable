import { LightningElement, track } from 'lwc';

export default class LwcTableForm extends LightningElement {
  //  @track save;
    @track listOfData =  [
        {
            index : 0,
            name : "",
            gender :"",
            designation :null,
            fullTime : null,
            salary : null,
            email : null,
            description : null,
            save : false,
            edit : true

        }
    ];


    keyIndex = 0;
    connectedCallback() {
        this.listOfData = [];
   //     this.edit = true;
   this.save = false;
        this.initData();
    }

    initData() {
        let listOfData = [];
        this.listOfData = [];
       
        this.createRow();
        // this.listOfData = listOfData;
    }

    // createRow(){
    //    let objData = {
    //         id : this.listOfData.length,
    //         name : "",
    //         gender :"",
    //         designation :null,
    //         fullTime : null,
    //         salary : null,
    //         email : null,
    //         description : null
    //     }
    //     this.listOfData.push(objData);
    //         // ++this.keyIndex;
    //         // var newItem = [{ id: this.keyIndex }];
    //         // this.listOfData = this.listOfData.concat(newItem);
      
    // }

    createRow() {
        console.log("createRow");

        let dataObjects = {};
        if (this.listOfData.length > 0) {
            dataObjects.index = this.listOfData[this.listOfData.length - 1].index + 1;
        }
        else {
            dataObjects.index = 1;
        }

        dataObjects.name = null;
        dataObjects.gender = null;
        dataObjects.designation = null;
        dataObjects.fullTime = null;
        dataObjects.salary = null;
        dataObjects.email = null;
        dataObjects.description = null;
        dataObjects.save = false;
        dataObjects.edit = true;
        console.log(typeof dataObjects.name);

        this.listOfData.push(dataObjects);
        //this.edit = false;
    }

    value = '';

    get options() {
        return [
            { label: 'Male', value: 'Male' },
            { label: 'Female', value: 'Female' },
        ];
    }

    addRowHandler() {
        console.log("addRowHandler");
        this.createRow();
    }



    // deleteHandler(event) {
    //     console.log("event.target.accessKey "+event.target.accessKey);
    //     if (this.listOfData.length >= 2) {
    //         this.listOfData.splice(event.target.accessKey,1);
    //     }
    // }

    deleteHandler(event) {
        let uniqueId = event.target.name;
        console.log("uniqueId "+uniqueId);
        let lstTempData = [];
        for(let i=0; i<this.listOfData.length; i++)
        {
            console.log(this.listOfData);
            if(this.listOfData[i].index != uniqueId)
            {
                lstTempData.push(this.listOfData[i]);
            }
        }
        this.listOfData = lstTempData;

        for (let i = 0; i < this.listOfData.length; i++) 
        {
            this.listOfData[i].index = i + 1;
        }
    }

    // deleteHandler(event) {
    //     let toBeDeletedRowIndex = event.target.name;
    //     console.log("toBeDeletedRowIndex "+toBeDeletedRowIndex);
    //     console.log("this.listOfData.length" + this.listOfData.length);
    //     console.log("toBeDeletedRowIndex" + toBeDeletedRowIndex);

    //     this.listOfData.splice(toBeDeletedRowIndex-1,1);

    //     for (let i = 0; i < this.listOfData.length; i++) {
    //         this.listOfData[i].index = i + 1;
    //     }
    // }

  
    nameHandler(event)
    {
        let rowIndex = event.target.dataset.id;
      
        this.listOfData[rowIndex-1].name = event.target.value;
        console.log("Name "+ JSON.parse(JSON.stringify(this.listOfData)));
        console.log("Name "+ this.listOfData[rowIndex-1]);
        
        
        // this.name = event.target.value;
        // console.log("Name "+ this.name);

      //this.listOfData[i].name = this.name;
        // this.listOfData[rowIndex].dataObjects.name = event.target.value;
        // console.log(" this.listOfData.dataObjects.name " +  this.listOfData.dataObjects.name);

        //this.listOfData[rowIndex].push(dataObjects);

        // fetch index of the row
        // fetch obj for that index
        // manipate value of the oj proprty
        //this.name = event
    }

   
    genderHandler(event)
    {
        let rowIndex = event.target.dataset.id;
        //this.gender = event.target.value;
        this.listOfData[rowIndex-1].gender = event.target.value;
    }

    

    get designationOptions() {
        return [
            { label: 'Trainee', value: 'Trainee' },
            { label: 'Developer', value: 'Developer' },
            { label: 'Senior Developer', value: 'Senior Developer' },
            { label: 'Manager', value: 'Manager' },
            { label: 'Senior Manager', value: 'Senior Manager' },
        ];
    }

    designationHandler(event)
    {
        let rowIndex = event.target.dataset.id;
        this.listOfData[rowIndex-1].designation = event.target.value;
    }
        //this.designation = event.target.value;
        
    

    fullTimeHandler(event)
    {
        let rowIndex = event.target.dataset.id;
        console.log("rowIndex full time "+ rowIndex);
        this.listOfData[rowIndex-1].fullTime = event.target.checked;
        console.log(" this.listOfData[rowIndex-1].fullTime  "+  this.listOfData[rowIndex-1].fullTime );
       // this.fullTime = event.target.checked;
    }

    salaryHandler(event)
    {
        let rowIndex = event.target.dataset.id;
        this.listOfData[rowIndex-1].salary = event.target.value;
        //this.salary = event.target.value;
    }

    emailHandler(event)
    {
        let rowIndex = event.target.dataset.id;
        this.listOfData[rowIndex-1].email = event.target.value;
        //this.email = event.target.value;
    }

    descriptionHandler(event)
    {
        let rowIndex = event.target.dataset.id;
        this.listOfData[rowIndex-1].description = event.target.value;
        //this.description = event.target.value;
    }

    

    saveHandler(event)
    {
        let toBeSavedRowIndex = event.target.name;

        if( this.listOfData[toBeSavedRowIndex-1].name && 
            this.listOfData[toBeSavedRowIndex-1].gender &&
            this.listOfData[toBeSavedRowIndex-1].designation && 
            this.listOfData[toBeSavedRowIndex-1].salary &&  
            this.listOfData[toBeSavedRowIndex-1].email && 
            this.listOfData[toBeSavedRowIndex-1].description)
        {
            // this.listOfData[toBeSavedRowIndex-1].name = this.name;
            // this.listOfData[toBeSavedRowIndex-1].gender = this.gender;
            // this.listOfData[toBeSavedRowIndex-1].designation = this.designation;
            // this.listOfData[toBeSavedRowIndex-1].fullTime = this.fullTime;
            // this.listOfData[toBeSavedRowIndex-1].salary = this.salary;
            // this.listOfData[toBeSavedRowIndex-1].email = this.email;
            // this.listOfData[toBeSavedRowIndex-1].description = this.description;
            this.listOfData[toBeSavedRowIndex-1].save = true;
            this.listOfData[toBeSavedRowIndex-1].edit = false;
            // this.listOfSavedData[toBeSavedRowIndex-1].name = this.name;
            // this.listOfSavedData[toBeSavedRowIndex-1].gender = this.gender;
            // this.listOfSavedData[toBeSavedRowIndex-1].designation = this.designation;
            // this.listOfSavedData[toBeSavedRowIndex-1].fullTime = this.fullTime;
            // this.listOfSavedData[toBeSavedRowIndex-1].salary = this.salary;
            // this.listOfSavedData[toBeSavedRowIndex-1].email = this.email;
            // this.listOfSavedData[toBeSavedRowIndex-1].description = this.description;

            var btn = component.find(toBeSavedRowIndex); // get button component
            event.getSource().set("Save", "Edit");
           // btn.set("Save", "Edit"); // set the title
            
        }
        else
        {
            this.listOfData[toBeSavedRowIndex-1].save = false;
            this.listOfData[toBeSavedRowIndex-1].edit = true;
        }
      
      //  console.log(JSON.parse(JSON.stringify(this.listOfData)));
        console.log(JSON.parse(JSON.stringify(this.listOfSavedData)));
       
    }

    editHandler(event)
    {
        let toBeSavedRowIndex = event.target.name;
        console.log("toBeSavedRowIndex "+ toBeSavedRowIndex);
        this.listOfData[toBeSavedRowIndex-1].edit = true;
        this.listOfData[toBeSavedRowIndex-1].save = false;

    }

}