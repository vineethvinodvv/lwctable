import { LightningElement, wire, track } from 'lwc';
import getAccounts from '@salesforce/apex/WireTask1Handler.getAccounts';

export default class WireTask1 extends LightningElement {

    @track searchKey;
    @track accountList;
    // @wire(getAllAccounts)
    // accounts;
    
    // get response()
    // {
    //     console.log("this.accounts "+ this.accounts)
    // }
    
    @wire(getAccounts, {searchKey :'$searchKey'})
    accountHandler({data, error}){
        if(data)
        {
            this.accountList = data;

        }

        if(error)
        {
            console.error(error);
        }
    }
   
    searchHandler(event)
    {
        this.searchKey = event.target.value;
    }

}