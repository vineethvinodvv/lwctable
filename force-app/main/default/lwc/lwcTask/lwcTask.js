import { LightningElement, track } from 'lwc';

export default class LwcTask extends LightningElement {

    @track firstName;
    @track lastName;
    @track gender;
    @track designation;
    @track fulltime;
    @track salary;
    @track email;
    @track description;
    @track submit;

    onFirstname(event)
    {
        this.firstName = event.target.value;
        console.log(this.firstName)
    }

    onLastname(event)
    {
        this.lastName = event.target.value;
    }

    onDesignation(event)
    {
        this.designation = event.target.value;
    }

    onSalary(event)
    {
        this.salary = event.target.value;
    }

    onEmail(event)
    {
        this.email = event.target.value;
    }

    onDescription(event)
    {
        this.description = event.target.value;
    }

    onGender(event)
    {
        this.gender = event.target.value;
    }
    onFullTime(event)
    {
        this.fulltime = event.target.checked;
    }

    submitHandle(event)
    {   
        console.log('this.firstName '+this.firstName );
        console.log('this.description '+this.description );
        console.log('this.email '+this.email);
        console.log('this.salary '+this.salary);
        console.log('this.designation '+this.designation);
        console.log('this.lastName '+this.lastName);
        console.log('this.gender '+ this.gender);

        if(this.firstName && this.description && this.email && this.salary && this.designation && this.lastName && this.gender )
        {
                this.submit = true;
        }
        else
        {
            this.submit = false;
        }
        

    }

}